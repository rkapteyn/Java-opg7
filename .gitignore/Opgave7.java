/* @file         Opgave7.java
 * @author       Jordy Perlee <jordy@perlee.net> (Based on work by Stephen Swatman)
 * @version      2018-10-11
 * @institution  Universiteit van Amsterdam
 * @course       Inleiding Programmeren
 * @assignment   Lindenmayer-systeem (week 7)
 *
 * A graphical interface to display the results of the Lindenmayer-system in.
 */

import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.Timer;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class Opgave7 extends JPanel implements ActionListener {
    private final static int HEIGHT = 800, WIDTH = 800;

    /* Increase the frames per second to speed up the drawing process.
     */
    private final static double FRAMES_PER_SECOND = 60.0;
    Timer timer = new Timer((int) (1000.0 / FRAMES_PER_SECOND), this);

    private ArrayList<Line2D.Double> lineBuffer;
    static boolean startProcessing = false;


    public static void main(String[] args) throws InterruptedException {
        /* Initialize the JFrame and JPanel used to construct the 
         * visual interface.
         */
        JFrame frame = new JFrame("Inleiding Programmeren - Lindenmayer");
        JPanel simulation = new Opgave7();
        simulation.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        frame.add(simulation);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();

        /* TODO:
         * Initialize your objects and generate your Lindenmayer String.
         * Only after this is done, startProcessing should be set to true.
         */     

        startProcessing = true;
    }


    /* Constructor for the Opgave7 class, which is used to construct
     * the graphical interface. An example of how to create lines to
     * be drawn on the screen can also be found here.
     */
    Opgave7() {
        timer.start();
        lineBuffer = new ArrayList<Line2D.Double>();

        /* As an example, create a line that goes starts at the origin 
         * and ends in the middle of the screen.
         */
        Point2D.Double point1 = new Point2D.Double(0, 0);
        Point2D.Double point2 = new Point2D.Double(WIDTH / 2, HEIGHT / 2);
        Line2D.Double example = new Line2D.Double(point1, point2);

        lineBuffer.add(example);
    }


    /* This method is called on every registered event. Currently, 
     * the only registered event is the timer defined at the top of
     * this class. Every time this method is called, it calls the
     * repaint method to update the content of the graphical interface.
     */
    public void actionPerformed(ActionEvent e) {
        repaint();
        Toolkit.getDefaultToolkit().sync();

    }

    
    /* The paint method draws the content of the listBuffer ArrayList onto
     * the screen. It is important to note that the screen is cleared 
     * every time this method is called, thus requiring us to redraw
     * all the lines in the buffer. This might seem counter-intuitive now,
     * but it will all be made clear why this is the default behavior
     * during the course 'Graphics and Game Technology' in year 2. ;-)
     */
    public void paint(Graphics graphics) {
        /* Graphics objects are typically 3D objects. Since we're working in
         * only two dimensions, we cast it to a 2D representation of itself.
         */
        Graphics2D graphics2D = (Graphics2D) graphics;
        
        /* Ensure that the window is always cleared by clearing a rectangle
         * that is the size of the window itself.
         */
        graphics2D.clearRect(0, 0, WIDTH, HEIGHT);

        if (startProcessing) {
            /* TODO:
             * Here you can have the next step of your Lindenmayer-system 
             * processed.
             */    
        }

        /* Set the width and color of the line 
         */
        graphics2D.setStroke(new BasicStroke(2));
        graphics2D.setColor(new Color(0.0F, 0.6F, 0.1F));

        /* Draw all the lines in the buffer. 
         */
        for (Line2D.Double line : lineBuffer) 
            graphics2D.draw(line);  
    }
}

