import java.util.Map;
import java.util.HashMap;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.Properties;

class Eigenschappen {

    private String axiom;
    private Map<String, String> rules;
    private int recursionDepth;

    public Eigenschappen(String fileName) throws IOException, FileNotFoundException, InvoerException {
        Properties prop = new Properties();
        InputStream input = new FileInputStream(fileName);
        prop.load(input);
        axiom = prop.getProperty("axiom");

        rules = new HashMap();
        String[] rulesStrings = prop.getProperty("rules").split(",");
        for (int i = 0; i < rulesStrings.length; i++) {
            String[] parts = rulesStrings[i].split(" ");
            rules.put(parts[0], parts[1]);
        }

        recursionDepth = Integer.parseInt(prop.getProperty("recursion.depth"));
        input.close();
    }

    public String getAxiom() {
        return axiom;
    }

    public Map getRules() {
        return rules;
    }

    public int getrecursionDepth() {
        return recursionDepth;
    }

    public static void main(String[] args) throws IOException, InvoerException {
        if (args.length != 1) throw new InvoerException(); 
        Eigenschappen e = new Eigenschappen(args[0]);
        System.out.println("axiom: " + e.getAxiom());
        System.out.println("rules: " + e.getRules().toString());
        System.out.println("depth: " + e.getrecursionDepth());
    }
}

class InvoerException extends RuntimeException {
    public String getMessage() {
        return("Usage: java Opgave7 inputfile");
    }
}