import java.util.Map;
import java.util.HashMap;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.Properties;

class Eigenschappen {

    private String axiom;
    private Map<String, String> rules;
    private int recursionDepth;
    private Double posX;
    private Double posY;
    private Double angle;
    private int lineLength;
    private int lineWidth;
    private Double angleMod;

    public Eigenschappen(String fileName) throws IOException, FileNotFoundException, InvoerException {
        Properties prop = new Properties();
        InputStream input = new FileInputStream(fileName);
        prop.load(input);
        axiom = prop.getProperty("axiom");

        rules = new HashMap();
        String[] rulesStrings = prop.getProperty("rules").split(",");
        for (int i = 0; i < rulesStrings.length; i++) {
            String[] parts = rulesStrings[i].split(" ");
            rules.put(parts[0], parts[1]);
        }

        recursionDepth = Integer.parseInt(prop.getProperty("recursion.depth"));
        posX = Double.parseDouble(prop.getProperty("turtle.pos.x"));
        posY = Double.parseDouble(prop.getProperty("turtle.pos.y"));
        angle = Double.parseDouble(prop.getProperty("turtle.angle"));
        lineLength = Integer.parseInt(prop.getProperty("turtle.line.length"));
        lineWidth = Integer.parseInt(prop.getProperty("turtle.line.width"));
        angleMod = Double.parseDouble(prop.getProperty("turtle.angle.modifier"));
        input.close();
    }

    public String getAxiom() {
        return axiom;
    }

    public int getRecursionDepth() {
        return recursionDepth;
    }

    public Map getRules() {
        return rules;
    }

    public Double getPosX() {
        return posX;
    }

    public Double getPosY() {
        return posY;
    }

    public Double getAngle() {
        return angle;
    }

    public int getLineLength() {
        return lineLength;
    }

    public int getLineWidth() {
        return lineWidth;
    }

    public Double getAngleMod() {
        return angleMod;
    }

    public static void main(String[] args) throws IOException, InvoerException {
        if (args.length != 1) throw new InvoerException(); 
        Eigenschappen e = new Eigenschappen(args[0]);
        System.out.println("axiom: " + e.getAxiom());
        System.out.println("rules: " + e.getRules().toString());
        System.out.println("depth: " + e.getRecursionDepth());
        System.out.println("length: " + e.getLineLength());
        System.out.println("angle: " + e.getAngle());
    }
}

class InvoerException extends RuntimeException {
    public String getMessage() {
        return("Usage: java Opgave7 inputfile");
    }
}