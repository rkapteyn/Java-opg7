import java.util.Map;
import java.io.IOException;
import java.util.HashMap;

class Lindenmayer {
    private String axiom;
    private Map<String, String> rules;
    private int recursionDepth;
    private String result;

    public Lindenmayer(Eigenschappen e) {
        this.axiom = e.getAxiom();
        this.rules = e.getRules();
        this.recursionDepth = e.getRecursionDepth();
    }

    private String nextRecursion(String sentence, int actualRecursionDepth) {
        if (actualRecursionDepth > recursionDepth) return sentence;
        String newSentence = "";
        for (int i = 0; i < sentence.length(); i++) {
           String letter = sentence.substring(i, i + 1);
           boolean found = false;
           for (Map.Entry<String,String> entry : rules.entrySet()) {
               if (letter.compareTo(entry.getKey()) == 0) {
                    newSentence += entry.getValue();
                    found = true;
                    break;
               }
           }
           if (!found) newSentence += letter;
        }
        if (actualRecursionDepth == recursionDepth) {
            result = newSentence;
        }
        else {
            nextRecursion(newSentence, actualRecursionDepth + 1);
        }
        return result;
    }

    public String calculate() {
        return nextRecursion(axiom, 1);
    }

    public static void main(String[] args) throws IOException {
        Eigenschappen e = new Eigenschappen("invoer/tree.properties");
        Lindenmayer l = new Lindenmayer(e);
        System.out.println("result: " + l.calculate());
    }

}