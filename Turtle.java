import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.Timer;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.BasicStroke;
import java.lang.Math.*;

class Turtle {
    private ArrayList<TurtleState> stack;

    private Double posX;
    private Double posY;
    private Double angle;
    private int lineLength;
    private int lineWidth;
    private Double angleMod;

    class TurtleState {
        Double x, y, a;

        public TurtleState(Double x, Double y, Double a) {
            this.x = x;
            this.y = y;
            this.a = a;
        }

        public Double getX() {
            return x;
        }

        public Double getY() {
            return y;
        }

        public Double getAng() {
            return a;
        }

    }

    public Turtle(Eigenschappen e) {
        posX = e.getPosX();
        posY = e.getPosY();
        angle = e.getAngle();
        lineLength = e.getLineLength();
        lineWidth = e.getLineWidth();
        angleMod = e.getAngleMod();
        stack = new ArrayList<TurtleState>(1000);
    }

    public void pushStack(TurtleState t) {
        stack.add(t);
    }

    public TurtleState popStack() {
        TurtleState t = stack.get(stack.size() - 1);
        stack.remove(stack.size() -1);
        return(t);
    }

    public Line2D.Double getLine(char c) {
        TurtleState cs = new TurtleState(posX, posY, angle);
        if (c == 'F' || c == 'G') {
            Point2D.Double oldPos = new Point2D.Double(posX, posY);
            posX += lineLength * java.lang.Math.cos(java.lang.Math.toRadians(angle));
            posY += lineLength * java.lang.Math.sin(java.lang.Math.toRadians(angle));
            Point2D.Double newPos = new Point2D.Double(posX, posY);
            return new Line2D.Double(oldPos, newPos);
        }

        if (c == '+') {
            angle += angleMod;
            if (angle > 360) angle -= 360;
        }

        if (c == '-') {
            angle -= angleMod;
            if (angle < 0) angle += 360;
        }

        if (c == '[') {
            pushStack(cs);
        }

        if (c == ']') {
            TurtleState t = popStack();
            posX = t.getX();
            posY = t.getY();
            angle = t.getAng();
        }
        return new Line2D.Double(new Point2D.Double(posX, posY), new Point2D.Double(posX, posY));
    }

}